extern crate osmpbfreader;
extern crate nav_types;

use std::env;
use std::fs::File;
use std::collections::HashMap;
use nav_types::WGS84;

struct Road {
    node_ids: Vec<osmpbfreader::NodeId>,
    nodes: Vec<osmpbfreader::Node>,
    maxspeed: Option<i32>,
    length_m: f32
}

fn get_maxspeed(obj: osmpbfreader::OsmObj) -> Option<i32> {
    let mut maxspeed: Option<i32> = None;

    for (tag, value) in obj.tags().iter() {
        if tag == "maxspeed" {
            let mut words = value.split_whitespace();
            maxspeed = match words.next() {
                Some(w) => match w.parse::<i32>() {
                    Ok(s) => Some(s),
                    Err(e) => {
                        println!("WARN: Could not parse maxspeed {}: {}", w, e);
                        None
                    }
                },
                None => {
                    println!("WARN: No maxpeed on {}", obj.way().unwrap().id.0);
                    None
                }
            };

            maxspeed = match words.next() {
                Some(unit) => match unit {
                    "mph" => Some(((maxspeed.unwrap_or(0) as f32) * 1.609344) as i32),
                    "kph" => maxspeed,
                    "km/h" => maxspeed,
                    _ => {
                        println!("[WARN] Invalid maxspeed unit {}", unit);
                        None
                    }
                },
                None => maxspeed
            };

            break;
        }
    }

    maxspeed
}

fn get_distance(a: osmpbfreader::Node, b: osmpbfreader::Node) -> f32 {
    let a = WGS84::new(a.decimicro_lat as f32 / 1e7, a.decimicro_lon as f32 / 1e7, 0.0);
    let b = WGS84::new(b.decimicro_lat as f32 / 1e7, b.decimicro_lon as f32 / 1e7, 0.0);

    a.distance(&b)
}

fn main() {
    if env::args().len() != 2 {
        println!("Usage: ./osm-average-speeds <osm.pbf file>");
        return;
    }

    println!("Opening osm.pbf file...");

    let file = File::open(env::args().nth(1).unwrap()).expect("Could not open osm.pbf file");
    let mut pbf = osmpbfreader::OsmPbfReader::new(file);
    
    let mut n_ways = 0;
    let mut n_ways_with_speed_limit = 0;
    let mut n_ways_with_over_two_nodes = 0;
    let mut total_distance = 0.0;
    let mut average_speed: f32 = 0.0;

    println!("Parsing roads...");
    let mut roads: Vec<Road> = vec!();
    let mut nodes: HashMap<osmpbfreader::NodeId, usize> = HashMap::new();
    for obj in pbf.par_iter() {
        let obj = obj.unwrap_or_else(|e| {
            panic!("Can't parse object: {:?}", e);
        });

        if !obj.is_way() || !obj.tags().contains("highway", "motorway") {
            continue;
        }

        n_ways += 1;

        let road = Road {
            node_ids: obj.way().unwrap().nodes.clone(),
            nodes: vec!(),
            maxspeed: get_maxspeed(obj),
            length_m: 0.0
        };

        if road.maxspeed != None {
            n_ways_with_speed_limit += 1;
            for node in &road.node_ids {
                nodes.insert(node.clone(), roads.len());
            }
            roads.push(road);
        }
    }

    println!("Finding nodes...");
    pbf.rewind().unwrap();
    for obj in pbf.par_iter() {
        let obj = obj.unwrap_or_else(|e| {
            panic!("Can't parse object: {:?}", e);
        });

        if obj.is_node() && nodes.contains_key(&obj.node().unwrap().id) {
            let road_index: usize = *nodes.get(&obj.node().unwrap().id).unwrap();
            let mut road = roads.get_mut(road_index).unwrap();
            road.nodes.push(obj.node().unwrap().clone());
        }
    }

    println!("Computing distances...");
    for road in &mut roads {
        let mut distance = 0.0;
        if road.nodes.len() > 1 {
            for i in 0 .. road.nodes.len() - 2 {
                distance += get_distance(road.nodes[i].clone(), road.nodes[i + 1].clone());
            }

            n_ways_with_over_two_nodes += 1;
        }

        road.length_m = distance;
        total_distance += distance;

        average_speed += distance * road.maxspeed.unwrap() as f32;
    }

    println!("Total motorways: {}", n_ways);
    if n_ways != 0 {
        println!("Total motorways with speed limit: {} ({} %)", n_ways_with_speed_limit, (n_ways_with_speed_limit * 100) / n_ways);
        println!("Total motorways with over two nodes: {} ({} %)", n_ways_with_over_two_nodes, (n_ways_with_over_two_nodes * 100) / n_ways_with_speed_limit);
        println!("Total distance: {} m", total_distance);
        println!("Average speed: {} km/h", average_speed / total_distance);
    }
}
