#!/usr/bin/env python3

import numpy as np
import matplotlib.pyplot as plt
from mpl_toolkits.basemap import Basemap
from matplotlib.patches import Polygon
from matplotlib.colors import rgb2hex

import csv

fig, ax = plt.subplots()

m = Basemap(lon_0=0,resolution='c')
m.fillcontinents(color='#eeeeee', lake_color='#ffffff')
m.drawmapboundary(fill_color='#ffffff')
m.readshapefile('../../shapes/st99_d00', 'states') # https://github.com/matplotlib/basemap/tree/master/examples
m.readshapefile('../../shapes/province', 'provincies') # https://www.weather.gov/gis/CanadianProvinces
m.readshapefile('../../shapes/CNTR_RG_20M_2016_4258', 'countries') # http://ec.europa.eu/eurostat/web/gisco/geodata/reference-data/administrative-units-statistical-units/countries#countries16

states = {}
with open('speed-limits-usa.csv') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',')
    for row in spamreader:
        states[row[0]] = float(row[1])
provincies = {}
with open('speed-limits-canada.csv') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',')
    for row in spamreader:
        provincies[row[0]] = float(row[1])
countries = {}
with open('speed-limits-europe.csv') as csvfile:
    spamreader = csv.reader(csvfile, delimiter=',')
    for row in spamreader:
        countries[row[0]] = float(row[1])

vmin = min(min(states.values()), min(provincies.values()), min(countries.values()))
vmax = max(max(states.values()), max(provincies.values()), max(countries.values()))

colors = {}
statenames = []
provincenames = []
countrynames = []
for shapedict in m.states_info:
    statename = shapedict['NAME']
    if statename in states:
        speed = states[statename]
        colors[statename] = plt.cm.Blues(np.sqrt((speed - vmin) / (vmax - vmin)))
    else:
        print("WARN: Missing US state " + statename)
    statenames.append(statename)
for shapedict in m.provincies_info:
    provincename = shapedict['NAME']
    if provincename in provincies:
        speed = provincies[provincename]
        colors[provincename] = plt.cm.Blues(np.sqrt((speed - vmin) / (vmax - vmin)))
    else:
        print("WARN: Missing Canada province " + provincename)
    provincenames.append(provincename)
for shapedict in m.countries_info:
    countryname = shapedict['NAME_ENGL'].split('\x00')[0]
    if countryname in countries:
        speed = countries[countryname]
        colors[countryname] = plt.cm.Blues(np.sqrt((speed - vmin) / (vmax - vmin)))
    else:
        print("WARN: Missing country " + provincename)
    countrynames.append(countryname)

for shape, seg in enumerate(m.states):
    statename = statenames[shape]
    if statename in colors:
        color = rgb2hex(colors[statename])
        poly = Polygon(seg, facecolor=color, edgecolor=color)
        ax.add_patch(poly)
for shape, seg in enumerate(m.provincies):
    provincename = provincenames[shape]
    if provincename in colors:
        color = rgb2hex(colors[provincename])
        poly = Polygon(seg, facecolor=color, edgecolor=color)
        ax.add_patch(poly)
for shape, seg in enumerate(m.countries):
    countryname = countrynames[shape]
    if countryname in colors:
        color = rgb2hex(colors[countryname])
        poly = Polygon(seg, facecolor=color, edgecolor=color)
        ax.add_patch(poly)


plt.title('Map of speed limits in North America and Europe')
plt.show()
