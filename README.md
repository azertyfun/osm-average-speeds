![Visualisation on the map](map_creation/speed-averages.png)

# osm-average-speeds

This tools allows you to visualize average motorway speed limits, based on [OSM](https://openstreetmap.org) data.

The data itself is generated in this Rust project, while the visualisation is done in the `map_creation/` folder using matplotlib's basemap.

## Usage

If you have the rust toolchain installed, all you need to do is get a [country's .osm.pbf file](https://download.geofabrik.de/), and run `cargo run /path/to/map.osm.pbf`.

If you want to update the visualization, you will need to update that country's data in the relevant CSV file in the `map_creation/` folder then run `python3 create_map.py`. If this is your first time running the tool, you will also need to download and place the shapefiles in the correct location; links and path are stored in the python file directly.

## Credits

The speed data is provided by the [openstreetmap contributors](https://www.openstreetmap.org/copyright). The shapefiles (used to show country/state/province outlines on the map) come from europa.eu (Country outlines), weather.gov (Canadian provinces) and the U.S. census bureau (United States).
